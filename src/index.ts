// Basic types:
let id: number = 5;
let company: string = "Traversy Media";
let isPublished: boolean = true;
let x: any = "Hello";

let ids: number[] = [1, 2, 3, 4, 5];
let arr: any[] = [1, true, 'Hello'];

// Tuple
let person: [number, string, boolean] = [1, 'Brad', true];

// Tuple Array
let employee: [number, string][]

employee = [
    [2, 'Brad'],
    [1, 'John'],
    [3, 'Jill']
]

// Union 
let pid: string | number;
pid = "22";

// Enum
enum Direction1 {
    Up,
    Down,
    Left,
    Right,
}
enum Direction2 {
    Up = "Up",
    Down = "Down",
    Left = "Left",
    Right = "Right",
}

// Objects
type User = {
    id: number,
    name: string
}

const user: User = {
    id: 1,
    name: 'G'
}

// Type Assertion
let cid: any = 1;

// let customerId = <number>cid;
// customerId = true;
let customerId = cid as number;

// Functions
function addNum(x: number, y: number): number {
    return x + y;
}

// void
function log(message: string | number): void {
    console.log(message);
}

// Interfaces
interface UserInterface {
    id: number,
    name: string
}

const user1: UserInterface = {
    id: 1,
    name: 'G'
}

type Point = number | string;

const p1: Point = 1;

